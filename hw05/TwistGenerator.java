/*
  This program inputs a twist length from the user
  and then outputs a twist with that desired length
  after checking that the user entered acceptable input 
*/

import java.util.Scanner;
public class TwistGenerator{
  public static void main (String[] args){
    
    Scanner kb = new Scanner (System.in);
    int length; //initialize length variable
    String message = "Enter a positive integer for twist length: "; //initialize messages strings
    String errorMessage = "Invalid input, please enter a positive integer.";
    System.out.print(message); //prompt input
    while(!kb.hasNextInt()||(length=kb.nextInt())<=0){ //check that it meets conditions of positive and int
        System.out.println(errorMessage); //if conditions not met, output error message
        System.out.print(message); //prompt for user input again
        kb.nextLine(); //eat line 
    }
    kb.nextLine(); //eat extra line
    
    String top = "\\ /"; //initialize parts of the twist 
    String middle = " x ";
    String bottom = "/ \\";
    int twistTimes = length/3; //calculate parts of twist 
    
    int twistMod = length%3; //see how many partial twists are leftover after full twists 
    switch(twistMod){ //switch to see which type of output. full twist, one part over, or two parts over. 
      case 0: //full twists 
        for(int k = 0;k<twistTimes;k++){ //loop to print twists for user input 
      System.out.print(top); //top part 
    }
    System.out.println(); //space output
    for(int k = 0;k<twistTimes;k++){ //same loop for middle section 
      System.out.print(middle);
    }
    System.out.println();
    for(int k = 0;k<twistTimes;k++){ //loop for bottom section 
      System.out.print(bottom);
    }
        System.out.println(); //space 
        break; 
      case 1: //one part over
        for(int k = 0;k<twistTimes;k++){ //top loop 
      System.out.print(top); 
    }
    System.out.print("\\"); //one part exra output 
    System.out.println();
    for(int k = 0;k<twistTimes;k++){ //middle loop 
      System.out.print(middle);
    }
    System.out.println(); //no middle extra because only one part over 
    for(int k = 0;k<twistTimes;k++){ //bottom loop 
      System.out.print(bottom);
    }
        System.out.print("/"); //bottom extra output 
        System.out.println();
        break;
      case 2: //two parts over 
        for(int k = 0;k<twistTimes;k++){ //top loop 
      System.out.print(top);
    }
    System.out.print("\\"); //extra output 
    System.out.println();
    for(int k = 0;k<twistTimes;k++){ //middle loop 
      System.out.print(middle);
    }
    System.out.print(middle); //extra middle output for two over 
    System.out.println();
    for(int k = 0;k<twistTimes;k++){ //bottom loop 
      System.out.print(bottom);
    }
        System.out.print("/"); //extra output 
        System.out.println();
        break;
    }    
  }
} 