/*
Eric Stieber
CSE2 210
This program inputs data on rainfall. input of land size and
rainfall amount is given and an output is calculated and displayed
of it in terms of cubic miles
*/
import java.util.Scanner;
public class Convert
{
  public static void main (String[] args)
  {
    Scanner kb = new Scanner (System.in); //user input
    System.out.print("Enter the affected area in acres: "); //prompt input of land size
    double acresAffected = kb.nextDouble(); //input taken and assigned to variable
    System.out.print("Enter the rainfall in the affected area: "); //input prompt for rainfall
    double  rainfall = kb.nextDouble(); //input given to variable 
    
    double cubicMiles = acresAffected*rainfall; //calculations started for cubic miles
    cubicMiles*=27154.2857; //calculation
    cubicMiles*=0.000000000000908169; //calculation 
    System.out.println(cubicMiles+" cubic miles"); //output line
    //end class
    
  }
}