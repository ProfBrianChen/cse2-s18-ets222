/*
Eric Stieber
CSE2 210
This program inputs dimensions of a pyramid and output is given for the volume 
inside of the pyramid
*/
import java.util.Scanner;
public class Pyramid
{
  public static void main (String[] args)
  {
    Scanner kb = new Scanner (System.in); //user input
    System.out.print("What is the length of the square side of the pyramid? "); //prompt dimension input square variable
    double squareSide = kb.nextDouble(); //square length taken and assigned to variable
    System.out.print("What is the height of the pyramid? "); //prompt user input height
    double height = kb.nextDouble(); //input assigned to variable
    double volume = (squareSide*squareSide*height)/3.0; //calculate the volume using formula
    System.out.println("The volume inside the pyramid is: "+volume); //output
  }
}