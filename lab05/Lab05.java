import java.util.Scanner;
public class Lab05{
  public static void main (String[] args){
    Scanner kb = new Scanner (System.in);
    int courseNumber=0, numStudents=0, meetingTime=0, numMeetings=0; //initialize variables
    String departmentName, instructorName="";
    boolean check1=true, check2=true, check3=false, check4=false, check5 = false, check6 = false;
    do{
      System.out.print("Enter the course number: "); //loop to prompt input
      if(kb.hasNextInt()){ //check if it satisfies requirements
        courseNumber+=kb.nextInt(); //adds if it does
        check1=true; //changes conditional variable
      } else{
        check1=false;
        kb.next(); //otherwise repeat
        System.out.println("Invalid input, please enter an int.");
      }
    }while(!check1);
    
    do{
      System.out.print("Enter the number of students in the class: ");
      if(kb.hasNextInt()){
        numStudents+=kb.nextInt();
        check2=true;
      } else{
        check2=false;
        kb.next();
        System.out.println("Invalid input, please enter an int.");
      }
    }while(!check2);
    
    while(!check3){
      System.out.print("Enter the number of times the class meets a week: ");
      if(kb.hasNextInt()){
        numMeetings+=kb.nextInt();
        check3=true;
      } else{
        check3=false;
        kb.next();
        System.out.println("Invalid input, please enter an int.");
      }
    }
    
    while(!check4){
      System.out.print("Enter the time of day that class occurs: ");
      if(kb.hasNextInt()){
        meetingTime+=kb.nextInt();
        check4=true;
      } else{
        check4=false;
        kb.next();
        System.out.println("Invalid input, please enter an int.");
      }
    }
   
    System.out.print("What is the department name? ");
    departmentName=kb.nextLine();
    kb.nextLine();
    System.out.print("What is the name of the instructor? ");  
    instructorName+=kb.nextLine();
        
    System.out.println("The department name is "+departmentName); //output
    System.out.println("The name of the instructor is "+instructorName);
    System.out.println("The course number is "+courseNumber);
    System.out.println("The number of students in the class is "+numStudents);
    System.out.println("The number of times the class meets a week is "+numMeetings);
    System.out.println("The time of day that class occurs is "+meetingTime);
    
  } 
}