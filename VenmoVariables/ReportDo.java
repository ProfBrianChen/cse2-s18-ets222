/*
  Group A program to output data about september transactions
*/
import java.util.Scanner;
public class ReportDo{
  public static void main (String[] args){
    Scanner kb = new Scanner (System.in);
    int numTrans; //transaction number variable 
    System.out.print("How many outgoing transactions were completed in September? "); //output for num of transactions
    while(!kb.hasNextInt()||(numTrans=kb.nextInt())<=0){ //check for error input 
      System.out.println("Invalid input, please enter a non-negative integer"); //error message 
      System.out.print("How many outgoing transactions were completed in September? "); //prompt for re-enter 
      kb.nextLine(); //eat line 
    }
    kb.nextLine(); //eat line 
    int count = 1; //initialize variables for calculations
    double totalSent=0; 
    double highestSent=0;
    double average;
    double soloTrans=0;
    do{  
      System.out.print("Enter value of outgoing transaction "+count+": "); //prompot input of individual transactions 
      soloTrans=kb.nextInt(); //assign to variable 
      totalSent+=soloTrans; //add to total 
        if(soloTrans>highestSent){ //if this individial is highest yet 
          highestSent=soloTrans; //replace variables with that value 
        }      
      count++; //increment
    }while(count<=numTrans); //iterate as many times as transactions
    average = totalSent/numTrans; //calc average 
    System.out.println("In the month of September, you sent $"+totalSent+" total"); //output total data
    System.out.println("Your average transaction is $"+average); //output average data 
    System.out.println("Your largest transaction was $"+highestSent); //output highest transaction data 
    //end
  }
}