/*
Eric Stieber
Giavanna Tabbachino
Michalina Modzelewska
Ryan McGuiness
*/
public class VenmoVariables
{
  public static void main (String[] args)
  {
    double amountSent; //how much money you sent 
    double amountReceived; //how much money you received
    int accountNumber; //the unique ID number for your account
    int transactionNumber; //unique number for each transaction
    double transferFee; //how much you are charged in fees to transfer money to your account 
    double phoneNumber; //your phone number 
    double initialBankBal; //how much money is in your bank account
    String firstName; 
    String lastName;
    
    amountSent = 5.75; //initializing variables
    amountReceived = 7.05;
    accountNumber = 1235;
    transferFee = 0.75;
    phoneNumber = 9196478391.0;
    initialBankBal = 100.0;
      
    double withdrawalBal = initialBankBal-amountSent; //arithmetic to calc bank balance after withdrawl 
    double receiveBal = initialBankBal+amountReceived; //arithmetic to calc bank balance after money is sent to you
    
    System.out.println("The account number is " + accountNumber);
    System.out.println("The transaction number is "+transactionNumber);
    System.out.println("The phone number is " + phoneNumber);
    System.out.println("The initial bank balance is " + initialBankBal);
    System.out.println("The fee to transfer money is " + transferFee);
    System.out.println("In this transaction, you received $" + amountReceived);
    System.out.println("In this transaction, you sent $" + amountSent);
    System.out.println("The balance of your bank after the withdrawal is $" + withdrawalBal);
    System.out.println("The balance after the money received is $" + receiveBal);
  }
}