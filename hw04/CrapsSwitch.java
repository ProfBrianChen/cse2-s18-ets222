/*
Eric Stieber
CSE2 210
This program inputs a random value or user value for two 
die rolls and outputs the slang
*/
import java.lang.Math;
import java.util.Scanner;
public class CrapsSwitch
{
  public static void main (String[] args)
  {
    Scanner kb = new Scanner (System.in); //user input
    int firstDigit = 0; //initialize digit integers
    int secondDigit = 0;
    System.out.print("Would you like to randomly cast dice or input your own? type \"r\" for random, \"i\" for input: ");
    //input user choice to random roll or enter results
    String decision = kb.next(); //input decision to variable
    switch(decision) //check input decision
    {
      case "R": //random selection
      case "r": firstDigit += (int)(Math.random()*6+1);  //generate dice rolls 1-6
              secondDigit += (int)(Math.random()*6+1);
        break;
      case "I": //user input values
      case "i": System.out.print("Enter first digit: "); //prompt roll 1 input
              firstDigit += kb.nextInt(); //assign to variable
              System.out.print("Enter second digit: "); //prompt roll 2 input 
              secondDigit += kb.nextInt(); //assign to variable
        break;
      default:
        System.out.println("Error, unacceptable input. Please run the program again"); //error if decision not an option
        break;
    }
    switch(firstDigit) //check first digit to be a num 1-6
    {
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      break;
      default: //if not a num on die, error statement outputs
    {
       System.out.println("Error, unacceptable input. Please run the program again");
        break;
    }
    }
    switch(secondDigit) //check second digit to be a num 1-6
    {
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      break;
      default: //if num not on die then output error statement
    {
       System.out.println("Error, unacceptable input. Please run the program again");
        break;
    }
    }
    
    String digitPair = firstDigit+""+secondDigit; //create string of both digits to use in switch statements
    String slang; //initialize output variable slang
    switch(digitPair) //check the digit pair string
    {
      case "11": //value of digit pair
        slang = "Snake Eyes"; //slang terminology for that digit pair
        break;
      case "12":
        slang = "Ace Deuce";
        break;
      case "13":
        slang = "Easy Four";
        break;
      case "14":
        slang = "Fever Five";
        break;
      case "15":
        slang = "Easy Six";
        break;
      case "16":
        slang = "Seven Out";
        break;
      case "21":
        slang = "Ace Deuce";
        break;
      case "22":
        slang = "Hard Four";
        break;
      case "23":
        slang = "Fever Five";
        break;
      case "24":
        slang = "Easy Six";
        break;
      case "25":
        slang = "Seven Out";
        break;
      case "26":
        slang = "Easy Eight";
        break;
      case "31":
        slang = "Easy Four";
        break;
      case "32":
        slang = "Fever Five";
        break;
      case "33":
        slang = "Hard Six";
        break;
      case "34":
        slang = "Seven Out";
        break;
      case "35":
        slang = "Easy Eight";
        break;
      case "36":
        slang = "Nine";
        break;
      case "41":
        slang = "Fever Five";
        break;
      case "42":
        slang = "Easy Six";
        break;
      case "43":
        slang = "Seven Out";
        break;
      case "44":
        slang = "Hard Eight";
        break;
      case "45":
        slang = "Nine";
        break;
      case "46":
        slang = "Easy Ten";
        break;
      case "51":
        slang = "Easy Six";
        break;
      case "52":
        slang = "Seven Out";
        break;
      case "53":
        slang = "Easy Eight";
        break;
      case "54":
        slang = "Nine";
        break;
      case "55":
        slang = "Hard Ten";
        break;
      case "56":
        slang = "Yo-leven";
        break;
      case "61":
        slang = "Seven Out";
        break;
      case "62":
        slang = "Easy Eight";
        break;
      case "63":
        slang = "Nine";
        break;
      case "64":
        slang = "Easy Ten";
        break;
      case "65":
        slang = "Yo-leven";
        break;
      case "66":
        slang = "Boxcars";
        break;
      default: //if pair dne, output error message
        slang = "Error, unacceptable input. Please run the program again";
        
    }
    
    System.out.println(digitPair+"\t"+slang); //output pair, format a tab, then the slang for that pair
    
    //end 
  
  }
}