/*
Eric Stieber
CSE2 210
This program inputs a random value or user value for two 
die rolls and outputs the slang
*/
import java.lang.Math;
import java.util.Scanner;
public class CrapsIf
{
  public static void main (String[] args)
  {
    Scanner kb = new Scanner (System.in); //user input
    int firstDigit = 0; //initialize digit integers
    int secondDigit = 0;
    System.out.print("Would you like to randomly cast dice or input your own? type \"r\" for random, \"i\" for input: ");
    //input user choice to random roll or enter results
    String decision = kb.next();
    decision = decision.toLowerCase(); //assign choice to variable
    if(decision.equals("i")) //if user wants to input
    {
      System.out.print("Enter first digit: "); //prompt for values 
      firstDigit += kb.nextInt(); //assign to variables
      System.out.print("Enter second digit: ");
      secondDigit += kb.nextInt();
    }
    else if (decision.equals("r")) //if rand gen
    {
      firstDigit += (int)(Math.random()*6+1); //randNum 1-6 to variable
      secondDigit += (int)(Math.random()*6+1);
    }
    else //otherwise return error 
    {
      System.out.println("Error, unacceptable input. Please run the program again");
    }
    if(firstDigit>6||secondDigit>6||firstDigit<1||secondDigit<1) //if user enter dice value not within scope return error
    {
      System.out.println("Error, unacceptable input. Please run the program again");  
    }
    
    String slang; //initialize slang output 
    if(firstDigit==1&&secondDigit==1) //check conditions matching die roll
      slang = "Snake Eyes"; //assign the appropriate slang terminology
    else if((firstDigit==1&&secondDigit==2)||(secondDigit==1&&firstDigit==2))
      slang = "Ace Deuce";
    else if(firstDigit==2&&secondDigit==2)
      slang = "Hard Four";
    else if((firstDigit==3&&secondDigit==1)||(firstDigit==1&&secondDigit==3))
      slang = "Easy Four";
    else if((firstDigit==2&&secondDigit==3)||(firstDigit==3&&secondDigit==2)||(firstDigit==1&&secondDigit==4)||(firstDigit==4&&secondDigit==1))
      slang = "Fever Five";
    else if(firstDigit==3&&secondDigit==3)
      slang = "Hard Six";
    else if((firstDigit==2&&secondDigit==4)||(firstDigit==4&&secondDigit==2)||(firstDigit==1&&secondDigit==5)||(firstDigit==5&&secondDigit==1))
      slang = "Easy Six";
    else if((firstDigit==3&&secondDigit==4)||(firstDigit==4&&secondDigit==3)||(firstDigit==2&&secondDigit==5)||(firstDigit==5&&secondDigit==2)||(firstDigit==1&&firstDigit==6)||(firstDigit==6&&secondDigit==1))
      slang = "Seven Out";
    else if(firstDigit==4&&secondDigit==4)
      slang = "Hard Eight";
    else if((firstDigit==3&&secondDigit==5)||(firstDigit==5&&secondDigit==3)||(firstDigit==2&&secondDigit==6)||(firstDigit==6&&secondDigit==2))
      slang = "Easy Eight";
    else if((firstDigit==5&&secondDigit==4)||(firstDigit==4&&secondDigit==5)||(firstDigit==3&&secondDigit==6)||(firstDigit==6&&secondDigit==3))
      slang = "Nine";
    else if(firstDigit==5&&secondDigit==5)
      slang = "Hard Ten";
    else if((firstDigit==4&&secondDigit==6)||(firstDigit==6&&secondDigit==4))
      slang = "Easy Ten";
    else if((firstDigit==5&&secondDigit==6)||(firstDigit==6&&secondDigit==5))
      slang = "Yo-leven";
    else
      slang = "Boxcars";
    System.out.println(firstDigit+" "+secondDigit+"\t"+slang); //output die roll and slang terminology
  //end 
  }
}