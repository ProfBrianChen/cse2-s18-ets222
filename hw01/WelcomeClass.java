/*
Eric Stieber
hw01
CSE2-210
This program outputs a welcome message with special formatting along with my Lehigh Username
*/
public class WelcomeClass
{
public static void main (String[] args)
  {
  System.out.println("  -----------"); //formatting
  System.out.println("  | WELCOME |"); //welcome statement
  System.out.println("  -----------");  
  System.out.println("  ^  ^  ^  ^  ^  ^"); 
  System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\"); 
  System.out.println("<-E--T--S--2--2--2->"); //lehigh username with formatting
  System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
  System.out.println("  v  v  v  v  v  v");
  }
}