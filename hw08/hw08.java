/*
 * Eric Stieber CSE2
 * This program uses a string array that has a deck of cards given and prints it out, shuffles and prints, and 
 * "deals" a hand and prints until user stops
 * I practiced manipulating arrays and using them within methods/loops
 */
import java.util.Scanner;
import java.util.Random;
public class Shuffling{ 
    public static void main(String[] args) 
    { 
        Scanner scan = new Scanner(System.in);  
        String[] suitNames={"C","H","S","D"}; //suit names    
        String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; //cards in each suit
        String[] cards = new String[52]; //initialize arrays
        String[] hand = new String[5]; 
        int numCards = 5; //num of cards in a hand
        int again = 1; //condition for loop in main to repeat hand being dealt and printed
        int index = 51; //starting sport for dealing a hand
        for (int i=0;i<52;i++) //run through 52 times
        { 
            cards[i]=rankNames[i%13]+suitNames[i/13]; //assing that index to a card value 
        } 
        System.out.println("Deck of cards:"); //heading
        printArray(cards); //method call to print array
        shuffle(cards); //method call to shuffle cards in array
        printArray(cards); //print shuffled cards

        while(again == 1) //loop as many times as user wants
        { 
            hand = getHand(cards,index,numCards); //assign array 'hand' to the value from method call
            System.out.println("\nHand:"); //heading
            printArray(hand); //output hand
            System.out.println(); //spacing
            index = index - numCards; //decrease hand so it would be next cards after previous hand
            System.out.print("Enter \"1\" if you want another hand drawn "); //prompt for re-run
            again = scan.nextInt(); //input desicion
        } 
    } 

    public static void printArray(String[] cards) //nethod to output array
    {
        for(int i=0;i<cards.length;i++) //run through as many times as the array has an index
        {
            System.out.print(cards[i]+" "); //output array at that index
        }
        System.out.println(); //spacing
    }

    public static void shuffle(String[] list) //method to mix up cards in array
    {
        System.out.println("\nShuffled:"); //heading
        Random random = new Random(); //initialize random number
        for(int i=0;i<52;i++) //loop as many times as indexes 
        {
            int rnd = i+random.nextInt(52-i); //pick random index
            String temp = list[rnd]; //assing temp var to the value at that index
            list[rnd]=list[i]; //swap rand num
            list[i]=temp; //swap temp
        }
    }

    public static String[] getHand(String[] list, int index, int numCards) //method to print hand
    {
        String[] hand=new String[5];//array to hold value of hand
        for(int i=0;i<numCards;i++) //loop as many cards are in the hand
        {
            hand[i]=list[index]; //assign index with shuffled cards array starting from the top(last one)
            index--; //decrement to get the next card below it
        }
        return hand; //return hand array
    }
}

