/*
This program uses the scanner import to input user data about a dinner with friends
and calculates the bill split evenly among all members

Eric Stieber CSE2 Lab03
*/
import java.util.Scanner;
public class Check
{
  public static void main (String [] args)
  {
    Scanner kb = new Scanner (System.in); 
    System.out.print("Enter the original cost of the check in the form of xx.xx: "); 
    double checkCost = kb.nextDouble(); //input and assign check cost
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = kb.nextDouble(); //input and assign tip percentage 
    tipPercent /= 100; //converts to a decimal value
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = kb.nextInt(); //input and assign number of ppl in the group
    
    double totalCost; //total cost of bill for everyone
    double costPerPerson; //part of the bill for each person
    int dollars, dimes, pennies; //units of money to output cost per person
    totalCost = checkCost * (1+tipPercent); //bill + tip
    costPerPerson = totalCost / numPeople; //equal part of bill per person
    dollars = (int) costPerPerson; //cast cost into int for dollars
    dimes = (int) (costPerPerson*10)%10; //cast then calculate dimes
    pennies = (int)(costPerPerson*100)%10; //cast then calculate pennies
    System.out.println("Each person in the group owes $"+dollars+'.'+dimes+pennies); //output statement
    //end class
  }
}