public class Arithmetic
{
  public static void main (String[] args)
  {
    int numPants; //number of pairs of pants
    numPants = 3; 
    double pantsPrice;//cost per pair of pants
    pantsPrice = 34.98;
    int numShirts; //number of sweatshirts
    numShirts = 2;
    double shirtPrice; //cost per shirt
    shirtPrice = 24.99;
    int numBelts; //number of belts
    numBelts = 1;
    double beltPrice; //cost per belt
    beltPrice = 33.99;
    double paSalesTax; //state tax rate
    paSalesTax = 0.06; 
    
    double pantsCostTotal = pantsPrice*numPants; //calc cost of all clothing item 
    double shirtCostTotal = shirtPrice*numShirts; 
    double beltCostTotal = beltPrice*numBelts;  
    double taxOnPants = (pantsPrice*paSalesTax)*numPants; //tax on purchases of all clothing item
    double taxOnShirt = (shirtPrice*paSalesTax)*numShirts;
    double taxOnBelt = (beltPrice*paSalesTax)*numBelts;
    double totalCost = pantsCostTotal+shirtCostTotal+beltCostTotal; //total cost of all clothing items no tax 
    double totalTax = taxOnPants+taxOnShirt+taxOnBelt; //total tax from all items 
    double grandTotal = totalTax+totalCost; //total payment 
    
    System.out.println("The total cost for the pants without tax is $"+pantsCostTotal); //output statements with headings and rounding 
    System.out.println("the total cost for the shirts without tax is $"+shirtCostTotal);
    System.out.println("The total cost for the belts without tax is $"+beltCostTotal);
    System.out.println();
    System.out.println("The total tax on all the pants is $"+(int)(taxOnPants*100)/100.0);
    System.out.println("The total tax on all the shirts is $"+(int)(taxOnShirt*100)/100.0);
    System.out.println("The total tax on all the belts is $"+(int)(taxOnBelt*100)/100.0);
    System.out.println();
    System.out.println("The total cost of all purchases without tax is $"+totalCost);
    System.out.println("The total tax on all purchases is $"+(int)(totalTax*100)/100.0);
    System.out.println();
    System.out.println("The grand total for this purchase is $"+(int)(grandTotal*100)/100.0);
  }
}
