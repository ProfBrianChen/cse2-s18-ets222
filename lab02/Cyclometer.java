/*
Eric Stieber
9/5/18
CSE2-210
This program takes data for two bicycle trips and outputs info about the two trips
*/
public class Cyclometer
{
  public static void main (String[] args)
  {
    int secsTrip1 = 480, secsTrip2 = 3220, countsTrip1 = 1561, countsTrip2 = 9037; //variables initializing counts and seconds for both trips
    double wheelDiameter = 27.0; //length of a wheel through the middle to calculate distance traveled
    final double PI = 3.14159; //constant used to make calculations
    int feetPerMile = 5280; //num of feet in a mile for distance calculations
    int inchesPerFoot = 12; //num of inches in a foot for distance calculations
    int secondsPerMinute = 60; //number of seconds in a minute for time calculations
    double distanceTrip1, distanceTrip2, totalDistance; //name variables for output, given values after calculations
    
    System.out.println("Trip 1 took "+(secsTrip1/(double)secondsPerMinute)+" minutes and had "+countsTrip1+" counts"); //outputting time and distance info for trip 1
    System.out.println("Trip 2 took "+(secsTrip2/(double)secondsPerMinute)+" minutes and had "+countsTrip2+" counts"); //outputting time and distance info for trip 2
    
    distanceTrip1 = countsTrip1*wheelDiameter*PI; //calculates distance in inches
    distanceTrip1/=inchesPerFoot*feetPerMile; //distance in miles
    distanceTrip2 = countsTrip2*wheelDiameter*PI; //calulates distance in inches;
    distanceTrip2/=inchesPerFoot*feetPerMile; //distance in miles
    totalDistance = distanceTrip1+distanceTrip2; //calculates total distance 
    
    System.out.println("Trip 1 was "+distanceTrip1+" miles"); //outputs trip 1 info
    System.out.println("Trip 2 was "+distanceTrip2+" miles"); //outputs trip 2 info
    System.out.println("The total distance was "+totalDistance+" miles"); //outputs total distance
    
  }
}
