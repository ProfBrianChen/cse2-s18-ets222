/*
 Eric Stieber 
 CSE02 210
 This program randomly generates and outputs a card from a deck
*/
import java.lang.Math;
public class CardGenerator
{
  public static void main (String[] args)
  {    
    int cardNum = (int) (Math.random()*52);
    
  
    String suit; //initializes variables
    String ID;
    if(cardNum>=1&&cardNum<=13) //when num is in category
      suit = "Diamonds"; //this is suit string
    else if(cardNum>=14&&cardNum<=26) ///category
       suit = "Clubs"; //suit
    else if(cardNum>=27&&cardNum<=39) //category 
      suit = "Hearts"; //suit
    else //other
      suit = "Spades"; //last suit
    
    int cardNumMod = cardNum%13; //calculation to see if it is a face card
    
    switch(cardNumMod) {
      case 1: ID = "Ace"; //when calc is 1 it is ace
        break;
      case 11: ID = "Jack"; //11 results in jack
        break;
      case 12: ID = "Queen"; //12 is queen
        break;
      case 0: ID = "King"; //0 is king
        break;
      default:
          ID=""+cardNumMod; //if not a face then it is the number 
        break;
    }
        
        System.out.println("You picked the "+ID+" of "+suit); //output line
    
  }
}


